import Api from '../../Api'

const user = {
    namespaced:true,
    state:{
        users:[],
        user: {
            name: '',
            phohe: '',
            email: '',
            address: ''
        }
    },
    mutations: {
        GET_USER(state, users){
            state.users = users
        },
        CLEAR_FORM(state){
            state.user = {
                name: '',
                phohe: '',
                email: '',
                address: ''
            }
        }
    },
    actions: {
        getUser({commit}){
            return new Promise(resolve => {
                Api.get('/user')
                .then(response => {
                    commit('GET_USER',response.data.data.user)
                    resolve(response.data)
                })
            })
        },
        postUser({dispatch, commit, state}){
            return new Promise((resolve, reject) => {
                Api.post('/user',state.user)
                .then(response => {
                    commit('CLEAR_FORM')
                    dispatch('getUser').then(() => {
                        resolve(response.data)
                    })
                })
                .catch(error => {
                    if (error.response.status == 422) {
                        commit('SET_ERRORS', error.response.data.errors, { root: true })
                        reject(error.response)
                    }
                })
            })
        }
    },
    getters:{
        getUser(state){
            return state.users
        }
    }
}

export default user