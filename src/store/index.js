import { createStore } from 'vuex'

import user from './modules/user'

export default createStore({
    modules:{
        user
    },
    state: {
        errors:[]
    },
    mutations: {
        SET_ERRORS(state,payload){
            state.errors = payload
        },
        CLEAR_ERRORS(state){
            state.errors = []
        }
    }
})